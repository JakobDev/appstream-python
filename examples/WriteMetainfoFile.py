import appstream_python


def main():
    component = appstream_python.AppstreamComponent()
    component.id = "com.example.ExampleApp"

    component.urls["homepage"] = "https://example.com"
    component.urls["bugtracker"] = "https://example.com/bugtracker"

    component.save_file("com.example.ExampleApp.metainfo.xml")


if __name__ == "__main__":
    main()

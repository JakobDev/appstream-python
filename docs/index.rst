.. appstream-python documentation master file, created by
   sphinx-quickstart on Tue Apr 26 10:29:31 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to appstream-python's documentation!
============================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    modules/index
    examples/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
